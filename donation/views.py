from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.hashers import make_password
from .models import Donation
from news.models import News
from program.models import Program

def make_view(request, id):
    news = News.objects.filter(id=id).all()[0]
    program = Program.objects.filter(news=news).all()
    return render(request, 'donation.html', {"news":news, "program_list":list(program)})

def donation(request, id):
    if not request.user.is_authenticated:
        return redirect('/login/')
    if request.method == 'POST':
        return donate(request, id)
    return make_view(request, id)

def donate(request, id):
    if request.method == 'POST':
        if "program" in request.POST.keys():
            program = request.POST['program']
        else:
            program = None
        amount = request.POST['amount']
        bill_by = request.POST['bill_by']
        message = request.POST['message']
        hide = request.POST.get('hide', False)
        donation = Donation(user=request.user, amount=amount, bill_by=bill_by, message=message, hide=hide, program_id=program)
        donation.save()
    return make_view(request, id)

def donation_list(request):
	data = list(Donation.objects.all().order_by('-date'))
	return render(request, 'donation_list.html', {"data" : data})
