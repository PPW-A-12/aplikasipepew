from django.db import models
from django.utils import timezone
from account.models import Account
from program.models import Program


class Donation(models.Model):
    user = models.ForeignKey(Account, on_delete=models.CASCADE)
    program = models.ForeignKey(Program, on_delete=models.CASCADE, null=True)
    amount = models.IntegerField()
    bill_by = models.CharField(max_length=64)
    message = models.CharField(max_length=1024)
    hide = models.BooleanField()
    date = models.DateField(default=timezone.now, blank=True)
