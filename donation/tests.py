from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from urllib.parse import urlencode
from account.models import Account
from .models import Donation
from news.models import News
from program.models import Program

class DonationPageTestCase(TestCase):
	def setUp(self):
		data = {'user': '', 'program' : '', 'amount' : '30000000', 'bill_by' : 'Joni',
		'message' : 'Semoga cepat pulih', 'date' : '' }
		self.Donation = Donation(**data)
		self.news.save()

	def test_donation_page_table_true(self):
		response = self.client.get('/donation-list/')
		html_response = response.content.decode('utf8')
		self.assertEqual(response.status_code, 200)
		self.assertIn('30000000', html_response)
		self.assertIn('Joni', html_response)
		self.assertIn('Semoga cepat pulih', html_response)

class DonationPageTestCase(TestCase):

    def setUp(self):
        data = {'email': 'example_mail@gmail.com',
                'password': 'password',
                'full_name': 'full name',
                'date_of_birth': '2013-12-13'}
        user = Account.objects.create_user(**data)
        newsData = {'title': 'title',
                    'description': 'description',
                    'image': "http://drhealthnut.com/wp-content/uploads/2013/11/dummy-image-landscape-1024x585.jpg"}
        news = News.objects.create(**newsData)
        programData = {'name': 'name',
                       'news': news}
        program = Program.objects.create(**programData)
        donationData = {'user': user,
                        'amount': 1234,
                        'bill_by': 'bill by',
                        'message': 'message',
                        'hide': True}
        self.donation = Donation(**donationData)
        self.donation.save()

    # Mengecek adanya halaman donation.html
    def test_get_donation_page_with_login(self):
        self.client.login(email='example_mail@gmail.com', password='password')
        response = self.client.get('/donation/' + str(self.donation.id) + '/')
        self.assertEqual(response.status_code, 200)

    def test_get_donation_page_without_login(self):
        response = Client().get('/donation/' + str(self.donation.id) + '/')
        self.assertRedirects(response, '/login/', status_code=302,
                             target_status_code=200, fetch_redirect_response=True)

    def test_donation_success(self):
        pass

    def test_donation_failed(self):
        pass
