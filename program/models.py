from django.db import models
from news.models import News


class Program(models.Model):
    name = models.CharField(max_length=64)
    news = models.ForeignKey(News, on_delete=models.CASCADE)
