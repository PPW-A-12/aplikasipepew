from django.urls import include, path
from .views import index, hapusKomentar
#
urlpatterns = [
	path('', index, name='about'),
    path('hapus-komentar/', hapusKomentar, name='hapus-komentar'),
]