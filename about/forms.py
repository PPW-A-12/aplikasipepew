from django import forms
from .models import Komentar

class FormKomentar(forms.ModelForm):
    class Meta:
    	error_messages = {
    	'required': 'Tolong isi input ini',
    	}

    	model = Komentar
    	fields = ('komentar',)