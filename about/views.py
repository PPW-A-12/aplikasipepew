from django.shortcuts import render, redirect
from .models import Komentar
from django.http import JsonResponse
from .forms import FormKomentar
import requests
import urllib.request, json
from django.views.decorators.csrf import csrf_exempt


def index(request):
	if request.method == 'POST':
		form = FormKomentar(request.POST)
		if form.is_valid():
			form.save()
			return redirect('about')
		else:
			return redirect('about')
	else:
		form = FormKomentar()
		daftarKomentar = Komentar.objects.all()
		return render(request, 'about.html', {'form' : form, 'daftarKomentar' : daftarKomentar})

def hapusKomentar(request):
	if request.method == 'POST':
		Komentar.objects.all().delete()
		return redirect('about')
