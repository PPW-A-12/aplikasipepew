from django.contrib import admin
from django.urls import path, include
from news.views import home, detail
from account.views import signup, signin, signout
from donation.views import donation, donation_list
from django.conf.urls.static import static
from django.conf import settings


urlpatterns = [
    path('admin/', admin.site.urls),
    path('donation/<int:id>/', donation, name='donation'),
    path('donation-list/', donation_list, name='donation-list'),
    path('register/', signup, name='signup'),
    path('login/', signin, name='signin'),
    path('login-with-google/', include('social_django.urls', namespace='social')),
    path('logout/', signout, name='signout'),
    path('news/<int:id>/', detail, name='detail'),
    path('about/', include('about.urls')), 
    path('', home, name='home'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
