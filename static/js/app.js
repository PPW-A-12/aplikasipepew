const NEWS_DESCRIPTION_LIMIT = 500;
const NEWS_PER_PAGE = 3;
let filtered_news = {};
let news_count = 0;
let active_news_count = 0;
let page = 1;

const wrap_text = (text, link, id) => {
  let count = 0;
  let output_text = '';
  text.split(' ').forEach(word => {
    count += word.length;
    if (count > NEWS_DESCRIPTION_LIMIT) return;
    output_text += word + ' ';
  });
  return `<span style="white-space: pre-line" id="content_${id}">${output_text}<a href='${link}'>Read More</a></span>`;
};

const build_pagination = new_page => {
  if (new_page) page = new_page;
  let idx = 1;
  for (let i = 1; i <= news_count; i++) {
    if (filtered_news[i]) {
      $(`#news_${i}`).hide();
      continue;
    }
    if (page * NEWS_PER_PAGE - (NEWS_PER_PAGE - 1) <= idx && idx <= page * NEWS_PER_PAGE) {
      $(`#news_${i}`).show();
    } else {
      $(`#news_${i}`).hide();
    }
    idx++;
  }
  render_pagination();
};

const render_pagination = () => {
  let pagination = '';
  const page_count = Math.ceil(active_news_count / NEWS_PER_PAGE);
  for (let i = 1; i <= page_count; i++) {
    if (i === page) {
      pagination += `<button class="btn btn-success" disabled>${i}</button> `;
    } else {
      pagination += `<button class="btn btn-default" onclick='build_pagination(${i})'>${i}</button> `;
    }
  }
  $('#pagination').html(pagination);
  if (!page_count) $('#empty_news').show();
  else $('#empty_news').hide();
};

const filter_news = () => {
  active_news_count = 0;
  const keyword = $('#search')
    .val()
    .toLowerCase();
  for (let i = 1; i <= news_count; i++) {
    const str = $(`#content_${i}`)
      .html()
      .toLowerCase();
    if (str.indexOf(keyword) !== -1) {
      filtered_news[i] = false;
      active_news_count++;
    } else {
      filtered_news[i] = true;
    }
  }
  build_pagination(Math.min(page, Math.ceil(active_news_count / NEWS_PER_PAGE)));
};

const add_search_event = () => {
  active_news_count = news_count;
  filter_news();
  $('#search').on('keyup input propertychange paste change', filter_news);
};
