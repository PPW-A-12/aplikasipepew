from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from urllib.parse import urlencode
from .models import News


class NewsPageTestCase(TestCase):
    def setUp(self):
        data = {'title': 'Banjir',
                'description': 'Lorem Banjir Ipsum',
                'image': 'image.jpg'}
        self.news = News(**data)
        self.news.save()

    def test_home_page_contain_news(self):
        response = self.client.get('/')
        html_response = response.content.decode('utf8')
        self.assertEqual(response.status_code, 200)
        self.assertIn('Lorem Banjir Ipsum', html_response)

    def test_details_with_invalid_news_id(self):
        response = self.client.get('/news/' + str(self.news.id+1) + '/')
        self.assertRedirects(response, '/', status_code=302,
                             target_status_code=200, fetch_redirect_response=True)

    def test_details_with_valid_news_id(self):
        response = self.client.get('/news/' + str(self.news.id) + '/')
        html_response = response.content.decode('utf8')
        self.assertEqual(response.status_code, 200)
        self.assertIn('Lorem Banjir Ipsum', html_response)
