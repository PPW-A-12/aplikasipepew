from django.shortcuts import render, redirect
from .models import News
from program.models import Program


def home(request):
    data = list(News.objects.all().order_by('-date'))
    return render(request, 'home.html', {"data": data})


def detail(request, id):
    data = list(News.objects.filter(id=id))
    if data:
        data = data[0]
        program = list(Program.objects.filter(news_id=data.id))
        return render(request, 'news.html', {"data": data, "program": program})
    return redirect('/')
