from django.db import models
from django.utils import timezone


class News(models.Model):
    title = models.CharField(max_length=64)
    description = models.TextField()
    image = models.CharField(max_length=64)
    date = models.DateField(default=timezone.now, blank=True)
