from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.hashers import make_password
from .models import Account


def home(request):
    return render(request, 'home.html', {})


def signup(request):
    if request.method == 'POST':
        data = dict(request.POST.items())
        if 'csrfmiddlewaretoken' in data.keys():
            del data['csrfmiddlewaretoken']
        data['password'] = make_password(data['password'])
        user = Account(**data)
        try:
            user.full_clean()
            user.save()
            return render(request, 'register.html', {'info': 'Registration completed successfully'})
        except Exception as e:
            e = {k: v[0] for k, v in e.message_dict.items()}
            return render(request, 'register.html', e)
    return render(request, 'register.html', {})


def signin(request):
    if request.method == 'POST':
        email = request.POST['email']
        password = request.POST['password']
        user = authenticate(request, email=email, password=password)
        if user is None:
            return render(request, 'login.html', {'error': 'Invalid email/password.'})
        login(request, user)
        return redirect('/')
    return render(request, 'login.html', {})


def signout(request):
    logout(request)
    return redirect('/')
