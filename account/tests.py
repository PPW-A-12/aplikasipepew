from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from urllib.parse import urlencode
from .models import Account


class RegisterPageTestCase(TestCase):
    def setUp(self):
        pass

    def test_get_register_page(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)

    def test_submit_valid_data(self):
        data = urlencode({'email': 'example_mail@gmail.com',
                          'password': 'password',
                          'full_name': 'full name',
                          'date_of_birth': '2013-12-13'})
        response = self.client.post(
            '/register/', data, content_type='application/x-www-form-urlencoded')
        html_response = response.content.decode('utf8')
        self.assertEqual(response.status_code, 200)
        self.assertIn('Registration completed successfully', html_response)
        self.assertEqual(len(Account.objects.all()), 1)

    def test_submit_invalid_data(self):
        data = urlencode({'email': 'example_mail',
                          'password': 'password',
                          'full_name': 'full name',
                          'date_of_birth': '2013/12/13'})
        response = self.client.post(
            '/register/', data, content_type='application/x-www-form-urlencoded')
        html_response = response.content.decode('utf8')
        self.assertEqual(response.status_code, 200)
        self.assertIn('Enter a valid email address', html_response)
        self.assertIn(
            'value has an invalid date format. It must be in YYYY-MM-DD format', html_response)
        self.assertEqual(len(Account.objects.all()), 0)

    def test_submit_duplicate_email(self):
        data = urlencode({'email': 'example_mail@gmail.com',
                          'password': 'password',
                          'full_name': 'full name',
                          'date_of_birth': '2013-12-13'})
        self.client.post('/register/', data,
                         content_type='application/x-www-form-urlencoded')
        response = self.client.post(
            '/register/', data, content_type='application/x-www-form-urlencoded')
        html_response = response.content.decode('utf8')
        self.assertIn(
            'User with this Email address already exists.', html_response)


class LoginPageTestCase(TestCase):
    def setUp(self):
        data = {'email': 'example_mail@gmail.com',
                'password': 'password',
                'full_name': 'full name',
                'date_of_birth': '2013-12-13'}
        Account.objects.create_user(**data)

    def test_get_login_page(self):
        response = Client().get('/login/')
        html_response = response.content.decode('utf8')
        self.assertEqual(response.status_code, 200)
        self.assertIn('Login with Google', html_response)

    def test_login_success(self):
        data = urlencode({'email': 'example_mail@gmail.com',
                          'password': 'password'})
        response = self.client.post(
            '/login/', data, content_type='application/x-www-form-urlencoded')
        self.assertRedirects(response, '/', status_code=302,
                             target_status_code=200, fetch_redirect_response=True)

    def test_login_fail(self):
        data = urlencode({'email': 'example_mail@gmail.com',
                          'password': 'password2'})
        response = self.client.post(
            '/login/', data, content_type='application/x-www-form-urlencoded')
        html_response = response.content.decode('utf8')
        self.assertEqual(response.status_code, 200)
        self.assertIn('Invalid email/password.', html_response)
